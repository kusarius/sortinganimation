#include "step_by_step_merge_sort.h"

void StepByStepMergeSort::mergeSort(ThreadSafeArray& array, int start, int end)
{
    int* a = array.data();

    if (end - start < 2) return;
    if (end - start == 2)
    {
        if (a[start] > a[start + 1])
        {
            array.swap(start, start + 1);

            CHECK_FOR_STOP_AND_PAUSE

            STEP(start, start + 1)
        }

        return;
    }

    int c = start + (end - start) / 2;
    mergeSort(array, start, c);
    mergeSort(array, c, end);

    std::vector<int> temp;
    int p1 = start, p2 = c;
    while (temp.size() < end - start)
    {
        CHECK_FOR_STOP_AND_PAUSE

        if (p2 >= end || (p1 < c && a[p1] < a[p2]))
        {
            temp.push_back(a[p1]);
            p1++;
        }
        else
        {
            temp.push_back(a[p2]);
            p2++;
        }
    }

    for (int i = start; i < end; i++)
    {
        array.setAt(i, temp[i - start]);

        CHECK_FOR_STOP_AND_PAUSE

        STEP(i, i)
    }
}

void StepByStepMergeSort::sort(ThreadSafeArray& array)
{
    mergeSort(array, 0, array.length());
    emit done();
}
