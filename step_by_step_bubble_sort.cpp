#include "step_by_step_bubble_sort.h"

void StepByStepBubbleSort::sort(ThreadSafeArray& array)
{
    int* data = array.data();
    int len = array.length();

    for (int i = 0; i < len - 1; i++)
        for (int j = 0; j < len - i - 1; j++)
        {
            if (isStop)
            {
                emit done();
                return;
            }

            while (isPause) { }

            if (data[j] > data[j + 1])
            {
                array.swap(j, j + 1);

                STEP(j, j + 1)
            }
        }

    emit done();
}
