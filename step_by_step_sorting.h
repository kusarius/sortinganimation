#ifndef STEP_BY_STEP_SORTING_H
#define STEP_BY_STEP_SORTING_H

#include <QObject>
#include <thread>
#include <chrono>

#include "thread_safe_array.h"

#define CHECK_FOR_STOP_AND_PAUSE if (isStop) \
                                    return; \
                                 while (isPause);

#define STEP(i1, i2) emit step(i1, i2); \
                     std::this_thread::sleep_for(std::chrono::milliseconds(stepDelay));

class StepByStepSorting : public QObject
{
    Q_OBJECT

protected:

    bool isStop = false;
    bool isPause = false;

    int stepDelay = 0;

public:

    virtual void sort(ThreadSafeArray&);

    void stop();
    void pause();
    void resume();

    void setStepDelay(int);

signals:

    void step(int, int);
    void done();

};

#endif // STEP_BY_STEP_SORTING_H
