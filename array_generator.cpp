#include "array_generator.h"

std::vector<int> ArrayGenerator::generateRandom(int size)
{
    std::vector<int> array;
    for (int i = 1; i <= size; i++)
        array.push_back(i);

    std::random_shuffle(array.begin(), array.end());

    return array;
}

std::vector<int> ArrayGenerator::generateAlmostSorted(int size)
{
    std::vector<int> array;
    for (int i = 1; i <= size; i++)
        array.push_back(i);

    int numberOfSwaps = rand() % (size / 3) + 1;
    for (int i = 0; i < numberOfSwaps; i++)
    {
        int index = rand() % (size - maxOffset);
        std::swap(array[index], array[index + rand() % maxOffset + 1]);
    }

    return array;
}

std::vector<int> ArrayGenerator::generateReversed(int size)
{
    std::vector<int> array;
    for (int i = size; i >= 1; i--)
        array.push_back(i);

    return array;
}

std::vector<int> ArrayGenerator::generateEqualElements(int size)
{
    int numberOfBlocks = rand() % (maxBlocks - minBlocks) + minBlocks;
    int elemsPerBlock = size / numberOfBlocks;

    std::vector<int> array;
    for (int i = 0; i < numberOfBlocks - 1; i++)
        for (int c = 0; c < elemsPerBlock; c++)
            array.push_back(i + 1);

    // the last block can have more elements (if size % numberOfBlocks != 0)
    for (int c = 0; c < elemsPerBlock + size % numberOfBlocks; c++)
        array.push_back(numberOfBlocks);

    std::random_shuffle(array.begin(), array.end());

    return array;
}
