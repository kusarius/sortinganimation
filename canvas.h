#ifndef CANVAS_H
#define CANVAS_H

#include <QFrame>
#include <QPainter>
#include <QStyleOption>

#include "thread_safe_array.h"

class Canvas : public QFrame
{
    Q_OBJECT
public:
    Canvas(QWidget *parent, ThreadSafeArray& a);

    void setMarks(int m1, int m2);
    void deleteMarks();

protected:
    void paintEvent(QPaintEvent *event) override;

private:

    ThreadSafeArray& array;
    int mark1, mark2;

    const QColor markedStickColor = QColor("#fefe1e");
    const QColor unmarkedStickColor = QColor("#fcab13");

    // for supporting stylesheets
    QStyleOption opt;
    QPainter painter;
};

#endif // CANVAS_H
