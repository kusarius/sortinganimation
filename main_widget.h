#ifndef MAIN_WIDGET_H
#define MAIN_WIDGET_H

#include <QWidget>
#include <QLayout>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

#include <thread>
#include <fstream>

#include "canvas.h"
#include "array_generator.h"
#include "step_by_step_bubble_sort.h"
#include "step_by_step_quick_sort.h"
#include "step_by_step_merge_sort.h"
#include "step_by_step_insertion_sort.h"
#include "step_by_step_shell_sort.h"

#include "gif.h"

#define START_SORTING(TYPE) currentSorting = new TYPE; \
                            currentSorting->setStepDelay(ui->horizontalSliderSpeed->value()); \
                            thr = std::thread(&TYPE::sort, static_cast<TYPE*>(currentSorting), std::ref(array));

namespace Ui {
class Widget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

private slots:
    void on_pushButtonGenerateArray_clicked();

    void on_pushButtonStart_clicked();

    void repaintCanvas(int, int);

    void onSortingDone();

    void on_pushButtonStop_clicked();

    void on_horizontalSliderSpeed_valueChanged(int value);

    void on_pushButtonPause_clicked();

    void on_pushButtonResume_clicked();

    void on_pushButtonLoadFromFile_clicked();

    void on_pushButtonSaveGIF_clicked();

private:
    void updateUI();

    bool isArrayGenerated = false;
    bool isSortingNow = false;
    bool isPause = false;

    Ui::Widget *ui;
    Canvas* canvas;

    ThreadSafeArray array;
    StepByStepSorting* currentSorting = nullptr;

    std::vector<QImage> animationFrames;

    const int numbersToGenerate = 50;
};

#endif // MAIN_WIDGET_H
