#include "step_by_step_shell_sort.h"

void StepByStepShellSort::sort(ThreadSafeArray& array)
{
    int* arr = array.data();
    int n = array.length();

    for (int gap = n / 2; gap > 0; gap /= 2)
    {
        for (int i = gap; i < n; i += 1)
        {
            int temp = arr[i], j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
            {
                CHECK_FOR_STOP_AND_PAUSE

                STEP(j, j - gap)

                arr[j] = arr[j - gap];
            }

            arr[j] = temp;
        }
    }

    emit done();
}
