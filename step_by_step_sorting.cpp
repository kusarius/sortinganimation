#include "step_by_step_sorting.h"

void StepByStepSorting::sort(ThreadSafeArray &)
{
}

void StepByStepSorting::stop()
{
    isStop = true;
}

void StepByStepSorting::pause()
{
    isPause = true;
}

void StepByStepSorting::resume()
{
    isPause = false;

}

void StepByStepSorting::setStepDelay(int sd)
{
    stepDelay = sd;
}
