#ifndef STEP_BY_STEP_BUBBLE_SORT_H
#define STEP_BY_STEP_BUBBLE_SORT_H

#include "step_by_step_sorting.h"

class StepByStepBubbleSort : public StepByStepSorting
{
public:

    void sort(ThreadSafeArray&) override;
};

#endif // STEP_BY_STEP_BUBBLE_SORT_H
