#ifndef ARRAY_GENERATOR_H
#define ARRAY_GENERATOR_H

#include <vector>
#include <algorithm>

class ArrayGenerator
{
private:
    static const int maxOffset = 3;

    static const int minBlocks = 5;
    static const int maxBlocks = 10;

public:
    ArrayGenerator();

    static std::vector<int> generateRandom(int size);
    static std::vector<int> generateAlmostSorted(int size);
    static std::vector<int> generateReversed(int size);
    static std::vector<int> generateEqualElements(int size);
};

#endif // ARRAY_GENERATOR_H
