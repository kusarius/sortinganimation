#include "canvas.h"

Canvas::Canvas(QWidget *parent, ThreadSafeArray& a)
    : QFrame(parent), array(a), mark1(-1), mark2(-1)
{
}

void Canvas::setMarks(int m1, int m2)
{
    mark1 = m1;
    mark2 = m2;
}

void Canvas::deleteMarks()
{
    setMarks(-1, -1);
}

void Canvas::paintEvent(QPaintEvent*)
{
    if (array.length() == 0)
        return;

    int* data = array.data();
    int len = array.length();

    int stickWidth = width() / len;
    int heightPerUnit = height() / (*std::max_element(data, data + len));

    painter.begin(this);

    painter.setPen(QPen(QBrush(QColor("#2b88c4")), stickWidth >= 3 ? 1 : 0));

    // drawing style elements
    opt.init(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

    for (int i = 0; i < len; i++)
    {
        int stickHeight = data[i] * heightPerUnit - 1;
        painter.fillRect(i * stickWidth, height() - stickHeight, stickWidth, stickHeight,
            QColor((i == mark1 || i == mark2) ? markedStickColor : unmarkedStickColor));

        // draw border
        painter.drawRect(i * stickWidth - 1, height() - stickHeight, stickWidth, stickHeight);
    }

    painter.end();
}
