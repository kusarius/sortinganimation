#include "step_by_step_insertion_sort.h"

void StepByStepInsertionSort::sort(ThreadSafeArray& array)
{
    int* a = array.data();
    int len = array.length();
    int curr, prevIndex;

    for (int i = 1; i < len; i++)
    {
        curr = a[i];
        prevIndex = i - 1;
        while (prevIndex >= 0 && a[prevIndex] > curr)
        {
            a[prevIndex + 1] = a[prevIndex];
            a[prevIndex] = curr;

            CHECK_FOR_STOP_AND_PAUSE

            STEP(prevIndex, prevIndex + 1)

            prevIndex--;
        }
    }

    emit done();
}
