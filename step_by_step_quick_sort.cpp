#include "step_by_step_quick_sort.h"

void StepByStepQuickSort::quickSort(ThreadSafeArray& array, int start, int end)
{
    int* a = array.data();
    int s = start, e = end;
    int v = a[start + (end - start) / 2];

    while (s <= e)
    {
        CHECK_FOR_STOP_AND_PAUSE

        while (a[s] < v) s++;
        while (a[e] > v) e--;

        if (s <= e)
        {
            array.swap(s, e);
            s++;
            e--;

            STEP(s, e)
        }
    }

    if (start < e) quickSort(array, start, e);
    if (s < end) quickSort(array, s, end);
}

void StepByStepQuickSort::sort(ThreadSafeArray& array)
{
    quickSort(array, 0, array.length() - 1);
    emit done();
}
