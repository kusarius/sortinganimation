#ifndef STEP_BY_STEP_MERGE_SORT_H
#define STEP_BY_STEP_MERGE_SORT_H

#include "step_by_step_sorting.h"

class StepByStepMergeSort : public StepByStepSorting
{
private:

    void mergeSort(ThreadSafeArray&, int, int);

public:

    void sort(ThreadSafeArray&) override;
};

#endif // STEP_BY_STEP_MERGE_SORT_H
