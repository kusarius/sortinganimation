#include "main_widget.h"
#include "ui_main_widget.h"

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setFixedSize(size());

    canvas = new Canvas(this, array);
    canvas->setGeometry(0, 0, 600, 600);
    canvas->setStyleSheet("border: 1px solid darkgray; background-color: #2b88c4;");
}

MainWidget::~MainWidget()
{
    delete ui;
    delete canvas;
}

void MainWidget::updateUI()
{
    ui->groupBoxSorting->setEnabled(isArrayGenerated);
    ui->groupBoxArray->setEnabled(!isSortingNow && !isPause);
    ui->groupBoxAlgorithm->setEnabled(!isSortingNow && !isPause);
    ui->groupBoxSaving->setEnabled(isArrayGenerated);

    ui->pushButtonStart->setEnabled(!isSortingNow && !isPause);
    ui->pushButtonResume->setEnabled(isPause);
    ui->pushButtonStop->setEnabled(isSortingNow || isPause);
    ui->pushButtonPause->setEnabled(isSortingNow && !isPause);
}

void MainWidget::on_pushButtonGenerateArray_clicked()
{
    if (ui->radioButtonRandom->isChecked())
        array.setArray(ArrayGenerator::generateRandom(numbersToGenerate));
    else if (ui->radioButtonAlmostSorted->isChecked())
        array.setArray(ArrayGenerator::generateAlmostSorted(numbersToGenerate));
    else if (ui->radioButtonEqualElems->isChecked())
        array.setArray(ArrayGenerator::generateEqualElements(numbersToGenerate));
    else
        array.setArray(ArrayGenerator::generateReversed(numbersToGenerate));

    canvas->repaint();

    isArrayGenerated = true;
    updateUI();
}

void MainWidget::on_pushButtonStart_clicked()
{
    animationFrames.clear();

    std::thread thr;
    if (ui->radioButtonBubble->isChecked())
    {
        START_SORTING(StepByStepBubbleSort)
    }
    else if (ui->radioButtonQuick->isChecked())
    {
        START_SORTING(StepByStepQuickSort)
    }
    else if (ui->radioButtonMerge->isChecked())
    {
        START_SORTING(StepByStepMergeSort)
    }
    else if (ui->radioButtonInsertion->isChecked())
    {
        START_SORTING(StepByStepInsertionSort)
    }
    else if (ui->radioButtonShell->isChecked())
    {
        START_SORTING(StepByStepShellSort)
    }
    else return;

    connect(currentSorting, SIGNAL(done()), this, SLOT(onSortingDone()));
    connect(currentSorting, SIGNAL(step(int, int)), this, SLOT(repaintCanvas(int, int)));

    thr.detach();

    isSortingNow = true;
    updateUI();
}

void MainWidget::repaintCanvas(int mark1, int mark2)
{   
    canvas->setMarks(mark1, mark2);
    canvas->repaint();

    QPixmap pixmap(canvas->size());
    canvas->render(&pixmap);
    animationFrames.push_back(pixmap.toImage().convertToFormat(QImage::Format_RGBA8888));
}

void MainWidget::onSortingDone()
{
    repaintCanvas(-1, -1);

    isSortingNow = false;
    isPause = false;
    updateUI();
}

void MainWidget::on_pushButtonStop_clicked()
{
    array.clear();
    isArrayGenerated = false;
    onSortingDone();

    currentSorting->stop();
}

void MainWidget::on_horizontalSliderSpeed_valueChanged(int value)
{
    if (currentSorting != nullptr)
        currentSorting->setStepDelay(value);
}

void MainWidget::on_pushButtonPause_clicked()
{
    isPause = true;
    isSortingNow = false;
    updateUI();

    currentSorting->pause();
}

void MainWidget::on_pushButtonResume_clicked()
{
    isPause = false;
    isSortingNow = true;
    updateUI();

    currentSorting->resume();
}

void MainWidget::on_pushButtonLoadFromFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        "Choose the file with an array. Numbers must be separated by space. All numbers must be positive.",
        ".", "Text Files (*.txt)");

    if (!fileName.isNull())
    {
        std::vector<int> a;
        std::ifstream fin(fileName.toStdString());
        int value;

        try
        {
            while (fin >> value)
            {
                if (value <= 0)
                {
                    QMessageBox::information(this, "Animated Sorting", "All numbers must be positive!");
                    return;
                }

                a.push_back(value);
            }
        }
        catch (std::exception ex)
        {
            QMessageBox::information(this, "Animated Sorting", "An error occured while reading the file!");
            return;
        }

        if (a.size() == 0)
        {
            QMessageBox::information(this, "Animated Sorting", "Array size must be larger than 0!");
            return;
        }

        array.setArray(a);

        canvas->repaint();

        isArrayGenerated = true;
        updateUI();
    }
}

void MainWidget::on_pushButtonSaveGIF_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        "Save sorting as animated GIF", ".", "GIF Files (*.gif)");

    if (!fileName.isNull())
    {
        std::thread saveThread([this, fileName] {
            GifWriter gw;
            GifBegin(&gw, fileName.toStdString().c_str(), canvas->width(), canvas->height(), 10);

            for (int i = 0; i < animationFrames.size(); i++)
                GifWriteFrame(&gw, animationFrames[i].bits(), canvas->width(), canvas->height(), 10);

            GifEnd(&gw);
        });

        saveThread.detach();
    }
}
