#-------------------------------------------------
#
# Project created by QtCreator 2018-08-05T10:53:34
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = AnimatedSorting
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += main.cpp \
        main_widget.cpp \
    canvas.cpp \
    thread_safe_array.cpp \
    array_generator.cpp \
    step_by_step_bubble_sort.cpp \
    step_by_step_sorting.cpp \
    step_by_step_merge_sort.cpp \
    step_by_step_quick_sort.cpp \
    step_by_step_insertion_sort.cpp \
    step_by_step_shell_sort.cpp \
    gif.cpp

HEADERS += main_widget.h \
    canvas.h \
    thread_safe_array.h \
    array_generator.h \
    step_by_step_sorting.h \
    step_by_step_bubble_sort.h \
    step_by_step_merge_sort.h \
    step_by_step_quick_sort.h \
    step_by_step_insertion_sort.h \
    step_by_step_shell_sort.h \
    gif.h

FORMS += \
        main_widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2

