#ifndef STEP_BY_STEP_QUICKSORT_H
#define STEP_BY_STEP_QUICKSORT_H

#include "step_by_step_sorting.h"

class StepByStepQuickSort : public StepByStepSorting
{
private:

    void quickSort(ThreadSafeArray&, int, int);

public:

    void sort(ThreadSafeArray&) override;
};

#endif // STEP_BY_STEP_QUICKSORT_H
