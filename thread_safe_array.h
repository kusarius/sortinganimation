#ifndef THREAD_SAFE_ARRAY
#define THREAD_SAFE_ARRAY

#include <vector>
#include <mutex>

#define LOCK_GUARD std::lock_guard<std::mutex> guard(mutex);

class ThreadSafeArray
{
private:

    std::mutex mutex;
    std::vector<int> a;

public:

    void setArray(std::vector<int> arr);
    void setAt(int, int);
    void add(int elem);
    void clear();
    int* data();
    void swap(int index1, int index2);
    int length();

};

#endif // THREAD_SAFE_ARRAY
