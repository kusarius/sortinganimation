#include "thread_safe_array.h"

void ThreadSafeArray::setArray(std::vector<int> arr)
{
    LOCK_GUARD
    a = arr;
}

void ThreadSafeArray::setAt(int i, int val)
{
    LOCK_GUARD
    a[i] = val;
}

void ThreadSafeArray::add(int elem)
{
    LOCK_GUARD
    a.push_back(elem);
}

void ThreadSafeArray::clear()
{
    LOCK_GUARD
    a.clear();
}

int* ThreadSafeArray::data()
{
    LOCK_GUARD
    return a.data();
}

void ThreadSafeArray::swap(int index1, int index2)
{
    LOCK_GUARD
    std::swap(a[index1], a[index2]);
}

int ThreadSafeArray::length()
{
    LOCK_GUARD
    return a.size();
}
